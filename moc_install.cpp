/****************************************************************************
** Meta object code from reading C++ file 'install.h'
**
** Created: Mon Jul 2 23:20:30 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "install.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'install.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_installDialog[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x0a,
      33,   14,   14,   14, 0x0a,
      51,   14,   14,   14, 0x0a,
      75,   14,   14,   14, 0x0a,
     101,   14,   14,   14, 0x0a,
     112,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_installDialog[] = {
    "installDialog\0\0Browser_clicked()\0"
    "Connect_clicked()\0connecturl(QModelIndex)\0"
    "returnIMname(QModelIndex)\0download()\0"
    "done(bool)\0"
};

const QMetaObject installDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_installDialog,
      qt_meta_data_installDialog, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &installDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *installDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *installDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_installDialog))
        return static_cast<void*>(const_cast< installDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int installDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: Browser_clicked(); break;
        case 1: Connect_clicked(); break;
        case 2: connecturl((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 3: returnIMname((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 4: download(); break;
        case 5: done((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
