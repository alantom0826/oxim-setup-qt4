/****************************************************************************
** Meta object code from reading C++ file 'oxim-setup.h'
**
** Created: Wed Jun 27 23:05:06 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "oxim-setup.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'oxim-setup.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_mainDialog[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      28,   11,   11,   11, 0x0a,
      44,   11,   11,   11, 0x0a,
      55,   11,   11,   11, 0x0a,
      78,   11,   11,   11, 0x0a,
      96,   11,   11,   11, 0x0a,
     113,   11,   11,   11, 0x0a,
     130,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_mainDialog[] = {
    "mainDialog\0\0AddIM_clicked()\0DelIM_clicked()\0"
    "DialogOK()\0findindex(QModelIndex)\0"
    "Default_clicked()\0Enable_clicked()\0"
    "Common_clicked()\0Attribute_clicked()\0"
};

const QMetaObject mainDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_mainDialog,
      qt_meta_data_mainDialog, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &mainDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *mainDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *mainDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_mainDialog))
        return static_cast<void*>(const_cast< mainDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int mainDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: AddIM_clicked(); break;
        case 1: DelIM_clicked(); break;
        case 2: DialogOK(); break;
        case 3: findindex((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 4: Default_clicked(); break;
        case 5: Enable_clicked(); break;
        case 6: Common_clicked(); break;
        case 7: Attribute_clicked(); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
